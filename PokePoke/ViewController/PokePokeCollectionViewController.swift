//
//  PokePokeCollectionViewController.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

private let reuseIdentifier = "PokeCell"
private let grass_color = "#FF46D0A7"
private let fire_color = "#FFFB6C6C"
private let water_color = "#FF77BDFE"
private let electric_color = "#FFFBCD66"

class PokePokeCollectionViewController: UICollectionViewController {
  
  //MARK: Properties
  let disposeBag = DisposeBag()
  var pokemons : [Pokemon] = []
  lazy var activityIndicator = UIActivityIndicatorView()
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    activityIndicator.color = .black
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    downloadPokemons()
  }
  
  //MARK: Functions
  func downloadPokemons(){
    activityIndicator.startAnimating()
    PokeNET.fetchPokemons()
      .asObservable()
      .observeOn(MainScheduler.instance)
      .do(onCompleted: {[weak self] in
        self?.activityIndicator.stopAnimating()
      })
      .subscribe(onNext: { [weak self] pokemon in
        self?.pokemons.append(pokemon)
        self?.collectionView.reloadData()
      }).disposed(by:disposeBag)
  }
  
  // MARK: UICollectionViewDataSource
  
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return pokemons.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let pokemon = pokemons[indexPath.row]
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PokemonCollectionViewCell
    cell.name.text = pokemon.name.capitalizingFirstLetter()
    cell.height.text = "Height: \(pokemon.height)ft"
    cell.weight.text = "Weight: \(pokemon.weight)lbs"
    if let url = URL(string: pokemon.spriteURL){
      cell.image.kf.setImage(with: url)
    }
    for type in pokemon.types{
      if type.slot == 1{
        cell.card.backgroundColor = getColor(forType: type)
        break
      }
    }
    return cell
  }
  
  // MARK: UICollectionViewDelegate
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let selectedPokemon = pokemons[indexPath.row]
    if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PokemonDetailViewController") as? PokemonDetailViewController {
      viewController.pokemon = selectedPokemon
      for type in selectedPokemon.types{
        if type.slot == 1{
          viewController.color = getColor(forType: type)
          break
        }
      }
      self.navigationController?.pushViewController(viewController, animated: true)
    }
  }
  override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    //if user dragged through 75% , download more.
    let count = self.pokemons.count - 10
    for index in self.collectionView.indexPathsForVisibleItems{
      if index.row > count {
        downloadPokemons()
        break
      }
    }
  }
  
  //MARK: Helpers
  private func getColor(forType type: Type) -> UIColor?{
    var hex: String
    switch type.name {
    case "grass":
      hex = grass_color
    case "electric":
      hex = electric_color
    case "fire":
      hex = fire_color
    case "water":
      hex = water_color
    default:
      hex = "#FFA06EB4"
    }
    let color = UIColor(hex:hex) ?? UIColor.darkGray
    return color
  }
}
