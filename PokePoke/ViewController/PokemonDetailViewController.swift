//
//  PokemonDetailViewController.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

class PokemonDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  //MARK: Properties
  var pokemon: Pokemon?
  var abilites: [Ability] = []
  let disposeBag: DisposeBag = DisposeBag.init()
  var color: UIColor?
  
  //MARK: Outlets
  @IBOutlet weak var speed: UILabel!
  @IBOutlet weak var special_attack: UILabel!
  @IBOutlet weak var special_defense: UILabel!
  @IBOutlet weak var defense: UILabel!
  @IBOutlet weak var attack: UILabel!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var tableView: UITableView!
  
  @IBOutlet weak var slotOne: UIView!
  @IBOutlet weak var slotThree: UIView!
  @IBOutlet weak var slotTwo: UIView!
  @IBOutlet weak var slotOneLabel: UILabel!
  @IBOutlet weak var slotTwoLabel: UILabel!
  @IBOutlet weak var slotThreeLabel: UILabel!
  @IBOutlet weak var cardTwo: Card!
  @IBOutlet weak var cardThree: Card!
  @IBOutlet weak var cardOne: Card!
  
  //MARK: LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.tableFooterView = UIView()
    // Do any additional setup after loading the view.
    updateUI()
    downloadAbilities()
    
  }
  
  //MARK: Functions
  private func downloadAbilities(){
    if let pokepoke = pokemon {
      Observable.from(pokepoke.abilities)
        .flatMap({ abilites in
          PokeNET.fetchAbility(fromURL: abilites.url)
        })
        .asObservable()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [weak self] ability in
          self?.abilites.append(ability)
          self?.tableView.reloadData()
        })
        .disposed(by: disposeBag)
    }
  }
  
  private func updateUI(){
        
    self.view.backgroundColor = color
    if let pokepoke = pokemon{
      
      slotOne.isHidden = true
      slotTwo.isHidden = true
      slotThree.isHidden = true
      
      for type in pokepoke.types{
        if type.slot == 1{
          self.slotOneLabel.text = type.name
          self.slotOne.isHidden = false
          self.slotOne.backgroundColor = color
          //self.cardOne.backgroundColor = color
        }
        else if type.slot == 2{
              self.slotTwoLabel.text = type.name
              self.slotTwo.isHidden = false
              //self.cardTwo.backgroundColor = color
              self.slotTwo.backgroundColor = color
        } else if type.slot == 3{
          self.slotThreeLabel.text = type.name
          self.slotThree.isHidden = false
          //self.cardThree.backgroundColor = color
          self.slotThree.backgroundColor = color
        }
      }
      
      for stat in pokepoke.stats.stats{
        switch stat.name {
        case "speed":
          speed.text = "\(stat.base)"
        case "special-defense":
          special_defense.text = "\(stat.base)"
        case "special-attack":
          special_attack.text = "\(stat.base)"
        case "attack":
          attack.text = "\(stat.base)"
        case "defense":
          defense.text = "\(stat.base)"
        default:
          break
        }
      }      
    }
    name.text = pokemon?.name.capitalizingFirstLetter()
    if let spriteURL = pokemon?.spriteURL, let url = URL(string:spriteURL){
      image.kf.setImage(with: url)
    }
  }
  
  //MARK: Tableview Delegates
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return abilites.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell",for: indexPath) as! DescriptionTableViewCell
    cell.ability_text.text = abilites[indexPath.row].effect
    return cell
  }
  
  
}
