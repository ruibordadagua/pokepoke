//
//  PokeService.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 30/04/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class PokeNET {
  
  static let API = "https://pokeapi.co/api/v2"
  static let pokemonEndpoint = "/pokemon"
  static let abilityEndpoint = "/ability"
  static var pokemonURLOffset = 0
  
  enum PokeError : Error {
    case invalidURL(String)
    case invalidParameter(String, Any)
    case invalidJSON(String)
  }
    
  static func fetchPokemons() -> Observable<Pokemon> {
    let query = ["offset":pokemonURLOffset,"limit":20]
    return request(endPoint: pokemonEndpoint,query: query)
      .map({ json -> [[String:Any]] in
      guard let result = json["results"] as? [[String:Any]] else {
        throw PokeError.invalidJSON(pokemonEndpoint)
      }
      pokemonURLOffset += 20
      return result
      })
      .map{ result -> [String] in
        result.compactMap({dict in
          guard let name = dict["name"] as? String else {
            return nil
          }
          return name
        })
    }.flatMap({pokenames in
      Observable.from(pokenames.map({ name in
        fetchPokemon(withName: name)
      }))
    })
    .merge(maxConcurrent: 4)
  }
  
  static func fetchAbility(fromURL url:String) -> Observable<Ability> {
    let endPoint = url.replacingOccurrences(of: API, with: "")
    return request(endPoint: endPoint).compactMap({result in
          Ability.init(abilityJson: result)
      })
  }
  
  static func fetchPokemon(withName name: String) -> Observable<Pokemon> {
    return request(endPoint: (pokemonEndpoint + "/" + name)).compactMap(Pokemon.init)
  }
  
  static func request(endPoint: String, query: [String:Any] = [:]) -> Observable<[String:Any]> {
    do{
      guard let url = URL(string: API)?.appendingPathComponent(endPoint),
        var components = URLComponents.init(url: url, resolvingAgainstBaseURL: true) else{
          throw PokeError.invalidURL(endPoint)
      }
      components.queryItems = try query.compactMap{ key, value in
        guard let v = value as? CustomStringConvertible else{
          throw PokeError.invalidParameter(key, value)
        }
        return URLQueryItem(name: key, value: v.description)
      }
      guard let finalURL = components.url else{
        throw PokeError.invalidURL(endPoint)
      }
      let request = URLRequest(url: finalURL)
      
      return URLSession.shared.rx.response(request: request)
        .map({ response,data in
          
          guard let json = try? JSONSerialization.jsonObject(with: data, options: []), let result = json as? [String:Any] else {
            throw PokeError.invalidJSON(endPoint)
          }
          return result
        })
    }catch{
      return Observable.empty()
    }
  }
}
