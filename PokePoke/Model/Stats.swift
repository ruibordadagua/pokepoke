//
//  Stats.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation

class Stats {
  let stats: [Stat]
  
  init(json: [[String:Any]]){
    self.stats = json.compactMap(Stat.init)
  }
}

class Stat {
  let name: String
  let base: Int
  
  init?(json: [String:Any]) {
    guard let stat = json["stat"] as? [String:Any],
      let name = stat["name"] as? String, let base = json["base_stat"] as? Int else {
        return nil
    }
    self.name = name
    self.base = base
  }
}
