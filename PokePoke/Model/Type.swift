//
//  Type.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 04/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation

class Type {
  let slot: Int
  let name: String
  let url: String
  init?(json: [String:Any]){
    guard let slot = json["slot"] as? Int, let type = json["type"] as? [String:Any], let name = type["name"] as? String, let url = type["url"] as? String else{
      return nil
    }
  self.name = name
  self.slot = slot
  self.url = url
  }
}
