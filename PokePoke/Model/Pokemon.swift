//
//  File.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 01/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import UIKit

class Pokemon {
  
  let id: Int
  let name: String
  let weight: Int
  let height: Int
  let spriteURL: String
  let stats: Stats
  let types: [Type]
  let abilities: [Ability]

  init?(json: [String: Any]) {
    guard let id = json["id"] as? Int,
        let name = json["name"] as? String,
        let height = json["height"] as? Int,
        let weight = json["weight"] as? Int,
      let spriteDict = json["sprites"] as? [String:Any],
      let sprite_front_default = spriteDict["front_default"] as? String,
      let stats = json["stats"] as? [[String:Any]],
      let types = json["types"] as? [[String:Any]],
      let abilities = json["abilities"] as? [[String:Any]] else {
      return nil
    }
    
    self.id = id
    self.name = name
    self.height = height
    self.weight = weight
    self.spriteURL = sprite_front_default
    self.stats = Stats(json:stats)
    self.types = types.compactMap(Type.init)
    self.abilities = abilities.compactMap{result in Ability.init(json:result)}
    
  }
  
}
