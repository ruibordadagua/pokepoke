//
//  Ability.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation

class Ability {
  let name: String
  var url: String
  var effect:String?
  
  init?(json: [String:Any]){
    guard let ability = json["ability"] as? [String:Any],let name = ability["name"] as? String, let url = ability["url"] as? String else {
      return nil
    }
    self.name = name
    self.url = url
  }
  
  init?(abilityJson: [String:Any]){
    guard let name = abilityJson["name"] as? String, let effect_entries = abilityJson["effect_entries"] as? [[String:Any]], let effect =  effect_entries[0]["effect"] as? String else {
      return nil
    }
    self.name = name
    self.effect = effect
    self.url = ""
  }
}
