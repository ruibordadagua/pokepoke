//
//  Card.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class Card: UIView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    updateUI()
  }
    
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    updateUI()
  }
  
  private func updateUI(){
    // corner radius
    self.layer.cornerRadius = 10    

  }
    

}
