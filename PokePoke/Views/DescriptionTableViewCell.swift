//
//  DescriptionTableViewCell.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 04/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

  @IBOutlet weak var ability_text: UILabel!
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
