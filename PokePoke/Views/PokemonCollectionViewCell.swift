//
//  PokemonCollectionViewCell.swift
//  PokePoke
//
//  Created by Rui Bordadagua on 03/05/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class PokemonCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var image: UIImageView!
  
  @IBOutlet weak var card: Card!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var height: UILabel!
  @IBOutlet weak var weight: UILabel!
  

}
